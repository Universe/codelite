# Maintainer: Jefferson Gonzalez <jgmdev@gmail.com>
# Contributor: Pyro Devil <p dot devil at gmail dot com>

pkgname=codelite-git
_gitname=codelite
pkgver=15.0.1.r33.g08e42248f
pkgrel=1
pkgdesc="A cross platform C/C++/PHP and Node.js IDE written in C++"
arch=('i686' 'x86_64' 'aarch64')
url="http://www.codelite.org/"
license=('GPL')
makedepends=('pkgconfig' 'cmake' 'ninja' 'clang' 'git')
depends=(
  'wxgtk3'
  'libedit'
  'libssh'
  'mariadb-libs'
  'ncurses'
  'xterm'
  'wget'
  'curl'
  'python2'
  'clang'
  'lldb'
  'hunspell'
)
optdepends=(
  'graphviz: callgraph visualization'
  'cscope: CScope Integration for CodeLite'
  'clang: compiler'
  'gcc: compiler'
  'gdb: debugger'
  'valgrind: debugger'
)
conflicts=('codelite' 'codelite-bin')
provides=('codelite')
source=(
  git://github.com/eranif/codelite.git
  http://repos.codelite.org/wxCrafterLibs/wxgui.zip
)
md5sums=('SKIP'
         '20f3428eb831c3ff2539a7228afaa3b4')
noextract=('wxgui.zip')


pkgver() {
  cd "${srcdir}/${_gitname}"
  git describe --tags --long | sed 's/\([^-]*-g\)/r\1/;s/-/./g'
}

prepare() {
  cd "${srcdir}/${_gitname}"
  mkdir -p build
}

build() {
  cd "${srcdir}/${_gitname}/build"

  CXXFLAGS="${CXXFLAGS} -fno-devirtualize"

  cmake -G "Ninja" -DCMAKE_BUILD_TYPE=Release \
    -DWITH_WX_CONFIG=/usr/bin/wx-config-gtk3 \
    -DENABLE_LLDB=1 -DWITH_MYSQL=0 \
    -DCMAKE_INSTALL_LIBDIR=lib \
    ..

  ninja
}

package() {
  cd "${srcdir}/${_gitname}/build"

  DESTDIR="${pkgdir}" ninja install
  install -m 644 -D "${srcdir}/wxgui.zip" "${pkgdir}/usr/share/codelite/wxgui.zip"
  install -m 644 -D "${srcdir}/${_gitname}/LICENSE" "${pkgdir}/usr/share/licenses/codelite/LICENSE"
}
